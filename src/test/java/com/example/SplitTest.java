package com.example;

import lombok.Value;
import lombok.extern.slf4j.Slf4j;
import org.apache.camel.EndpointInject;
import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.camel.RoutesBuilder;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.mock.MockEndpoint;
import org.apache.camel.processor.aggregate.GroupedExchangeAggregationStrategy;
import org.apache.camel.processor.aggregate.ShareUnitOfWorkAggregationStrategy;
import org.apache.camel.processor.aggregate.UseLatestAggregationStrategy;
import org.apache.camel.processor.aggregate.UseOriginalAggregationStrategy;
import org.apache.camel.test.junit4.CamelTestSupport;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

@Slf4j
public class SplitTest extends CamelTestSupport {

    @EndpointInject(uri = "mock:endMock")
    MockEndpoint endMock;

    @Test
    public void testRoute() throws Exception {
        //given
        List<Simple> simples = Arrays.asList(
                Simple.of("a", 1),
                Simple.of("a", 1),
                Simple.of("b", 2),
                Simple.of("a", 3),
                Simple.of("c", 4),
                Simple.of("b", 5)
        );
        endMock.expectedMinimumMessageCount(1);
        template().sendBody("direct:start", simples);
        endMock.assertIsSatisfied();

        endMock.getReceivedExchanges().forEach(exchange -> {
            Object body = exchange.getIn().getBody();
            log.info("Mock body: {}", body);
            if (body instanceof List) {
                log.info("Size: {}", ((List) body).size());
                ((List<Exchange>) body).forEach(e -> log.info("Wnętrze: {}", e.getIn().getBody()));
            }
        });
    }

    @Override
    protected RoutesBuilder createRouteBuilder() throws Exception {
        return new RouteBuilder() {
            @Override
            public void configure() throws Exception {
                from("direct:start")
//                        .split(body(), new Aggregator())
                        .split(body())
                            .parallelProcessing()
                            .filter(bodyAs(Simple.class).method("getName").isNotEqualTo("c"))
                            .filter(bodyAs(Simple.class).method("getName").isNotEqualTo("d"))
                            .process(exchange -> log.info("Splituje: {}", exchange.getIn().getBody()))
                        .bean(new BeanTest())
//                            .transform(bodyAs(Simple.class).method("getName"))
//                            .aggregate(body().method("getName"))
//                            .aggregationStrategy(new ShareUnitOfWorkAggregationStrategy(new Aggregator()))
//                            .completion().exchange(exchange -> {
//                                log.info("Sprawdzam exchange\n" +
//                                "headers: {}\n" +
//                                "properies: {}", exchange.getIn().getHeaders(), exchange.getProperties());
//                                log.info("Elo: {}", exchange.getProperty(Exchange.SPLIT_COMPLETE));
//                                return false;
//                            })
//                            .eagerCheckCompletion()
//                            .completionTimeout(3000)
                        .to("mock:endMock")
                ;
            }
        };
    }


}


//    from("direct:start")
//                        .split(body())
//                                .filter(bodyAs(Simple.class).method("getName").isNotEqualTo("c"))
//        .process(exchange -> log.info("Splituje: {}", exchange.getIn().getBody()))
//        .end()
//        .aggregate(body().method("getName"))
//        .aggregationStrategy(new GroupedExchangeAggregationStrategy())
////                        .completionPredicate(exchange -> {
////                            log.info("Properitsy: {}", exchange.getProperties());
////                            return true;
////                        })
//        .completionSize(2)
//        .to("mock:endMock")
//        ;
//        }