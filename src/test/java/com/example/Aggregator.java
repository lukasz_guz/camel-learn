package com.example;


import lombok.extern.slf4j.Slf4j;
import org.apache.camel.Exchange;
import org.apache.camel.processor.aggregate.GroupedExchangeAggregationStrategy;

@Slf4j
public class Aggregator extends GroupedExchangeAggregationStrategy {
    @Override
    public Exchange getValue(Exchange exchange) {
        return super.getValue(exchange);
    }

    @Override
    public Exchange aggregate(Exchange oldExchange, Exchange newExchange) {
        log.info("Aggreguje\n" +
                "headers: {}\n" +
                "properies: {}\n" +
                "body: {}", newExchange.getIn().getHeaders(), newExchange.getProperties(), newExchange.getIn().getBody());
        return super.aggregate(oldExchange, newExchange);
    }

    @Override
    public void onCompletion(Exchange exchange) {
        log.info("Sprawdzam wyłączenie");
        super.onCompletion(exchange);
    }
}
