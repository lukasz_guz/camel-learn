package com.example;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
//import rx.Observable;
//
//@Component
//public class DemoBean extends ObservableBody<String> {
//
//    private HelloBean helloBean;
//    private Logger log = LoggerFactory.getLogger(DemoBean.class);
//
//    @Autowired
//    public DemoBean(HelloBean helloBean) {
//        super(String.class);
//        this.helloBean = helloBean;
//    }
//
//    @Override
//    protected void configure(Observable<String> message) {
//        message
//                .doOnNext(s -> log.info("On next found: {}", s))
//                .map(String::toLowerCase)
//                .doOnNext(s -> log.info("Lower found: {}", s))
//                .map(helloBean::hello)
//                .subscribe(s -> log.info("Znalazłem bean: {}", s));
//    }
//}
