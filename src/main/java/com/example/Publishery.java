package com.example;

import lombok.extern.slf4j.Slf4j;
import org.apache.camel.CamelContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import rx.Observable;

import javax.annotation.PostConstruct;

@Component
@Slf4j
public class Publishery {

    private static final String URL = "rabbitmq://localhost/demo?username=admin&password=admin&queue=demo_publish_queue&autoDelete=false&routingKey=camel-rx-publish";


    @Autowired
    private CamelContext camelContext;

    @PostConstruct
    public void init() {
//        ReactiveCamel rx = new ReactiveCamel(camelContext);
//        rx.toObservable(URL, String.class)
//                .doOnUnsubscribe(() -> log.info("p - unsubscribe"))
//                .doOnNext(s -> log.info("Catch {}", s))
//                .map(Long::valueOf)
//                .doOnNext(s -> log.info("Long {}", s))
//                .subscribe(aLong -> log.info("Consume long: {}", aLong));


//        PublishSubject<String> p = PublishSubject.create();
//        p.doOnNext(s -> log.info("p - Catch  {}", s))
//                .doOnUnsubscribe(() -> log.info("p - unsubscribe"))
//                .map(Long::valueOf)
////                .onErrorReturn(throwable -> -1L)
////                .onExceptionResumeNext(Observable.empty())
////                .onErrorResumeNext(throwable -> Observable.just(0L) )
//                .doOnNext(s -> log.info("p- Long {}", s))
//                .subscribe(aLong -> log.info("p - Consume long: {}", aLong),
//                        throwable -> log.error("p - error", throwable));
//        p.onNext("1");
//        p.onNext("a");
//        p.onNext("3");
//
//
        Observable.just("1", "b", "3")
                .doOnUnsubscribe(() -> log.info("o - unsubscribe"))
                .doOnNext(s -> log.info("o - Catch  {}", s))
                .flatMap(value -> Observable.just(value)
                        .map(Long::valueOf)
                        .onExceptionResumeNext(Observable.empty())
                )
//                .onErrorReturn(throwable -> -1L)
//                .onErrorResumeNext(throwable -> Observable.just(0L) )
                .doOnNext(s -> log.info("o- Long {}", s))
//                .unsafeSubscribe(Subscribers.empty());
                .subscribe(aLong -> log.info("o - Consume long: {}", aLong),
                        throwable -> log.error("o - error", throwable));

    }
}
