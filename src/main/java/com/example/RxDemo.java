package com.example;

import org.springframework.context.annotation.Configuration;

@Configuration
public class RxDemo {

    public static final String URL = "rabbitmq://localhost/demo?username=admin&password=admin&queue=demo_queue&autoDelete=false&routingKey=camel-rx";
//    @Autowired
//    private CamelContext camelContext;

//    @Autowired
//    private DemoBean demoBean;
//
//    @Autowired
//    private HelloBean helloBean;

//    @PostConstruct
//    public void send() throws Exception {
//        addSpringBeanToRoute();
//        ReactiveCamel rx = new ReactiveCamel(camelContext);
//        rx.sendTo(Observable.just("elo")
//                .doOnNext(s -> System.out.println("Wysyłam: " + s)), URL);
//        rx.toObservable(URL, String.class)
//                .doOnNext(s -> System.out.println("On next found: " + s))
//                .map(String::toUpperCase)
//                .doOnNext(s -> System.out.println("UPPER found: " + s))
//                .subscribe(s -> System.out.println("Złapałem: " + s));
//    }
//
//
//    public void addSpringBeanToRoute() throws Exception {
//        camelContext.addRoutes(new RouteBuilder() {
//            @Override
//            public void configure() throws Exception {
//                from("rabbitmq://localhost/demo?username=admin&password=admin&queue=demo_consume&autoDelete=false&routingKey=demo_consume&concurrentConsumers=10")
////                        .bean(HelloBean.class);
//                        .process(demoBean);
////                        .to("seda:async?concurrentConsumers=2");
////                from("seda:async?concurrentConsumers=2")
////                        .process(demoBean);
//            }
//        });
//    }
}
